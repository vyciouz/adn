<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>ADN</title>
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<link href="style.css" rel="stylesheet">
<script
  src="https://code.jquery.com/jquery-3.3.1.slim.js"
  integrity="sha256-fNXJFIlca05BIO2Y5zh1xrShK3ME+/lYZ0j+ChxX2DA="
  crossorigin="anonymous"></script>
</head>

<body>
	<img class="logo" src="img/logo.png" width="54" height="67" alt=""/>
	<h1 class="titulo">Regístrate</h1>
	
	<img src="img/adncover.png" class="cover" />
	
	
<div class="iconos">
	

<form id="datos" action="evaluate_registration.php" method="post">
<?php
$banks = [
	1=>"BANAMEX",
	2=>"BANORTE",
	3=>"Debajo del colchón"
];
$fields = [
	// "user" => "Usuario",
	"id"			=> ["type"=>"text","legend"=> "Número de empleado", "array" => null],
	"name"			=> ["type"=>"text","legend"=> "Nombres", "array" => null],
	"surename1" 	=> ["type"=>"text","legend"=> "Apellido Paterno", "array" => null],
	"surename2" 	=> ["type"=>"text","legend"=> "Apellido Materno", "array" => null],
	"rs"			=> ["type"=>"text","legend"=> "Razón Social", "array" => null],
	"rfc" 			=> ["type"=>"text","legend"=> "RFC", "array" => null],
	"email" 		=> ["type"=>"text","legend"=> "Correo electrónico", "array" => null],
	"banco" 		=> ["type"=>"select","legend"=> "Banco", "array" => $banks],
	"cuenta_banco"	=> ["type"=>"text","legend"=> "Cuenta de banco", "array" => null],
	"clabe" 		=> ["type"=>"text","legend"=> "CLABE", "array" => null],
	"pwd" 			=> ["type"=>"password","legend"=> "Contraseña", "array" => null]
];

	foreach($fields as $key => $value ):
		if($value["type"] == "select"): ?>
			<select name="<?php echo $key?>" id="<?php echo $key?>">
			<?php foreach($banks as $key => $value ): ?>
			<option value="<?php echo $key?>"> <?php echo $value?> </option>
			<?php endforeach?>
			</select>
	<?php else: ?>
		<input placeholder="<?php echo $value["legend"]?>" type="<?php echo $value["type"]?>" id="<?php echo $key?>" name="submitted[<?php echo $key?>]" value="" size="60" maxlength="128" class="form-text">
		<?php endif; endforeach?>
	<p style = "font-weight:bold">Términos y condiciones</p>
	<p id="terms-body" style="text-align:justify; width: 80%; margin-left: 10%;">
	La Secretaría de Administración del Gobierno del Estado de Nuevo León, a través de la Dirección de Recursos Humanos, transferirá los datos personales que se detallan en el aviso de privacidad integral, los cuales se encuentran en los archivos físicos y bases de datos de esta Dirección, con la finalidad de acceder a la Plataforma ADN-GOB y los servicios que prestan en ella mediante la creación de un usuario y contraseña. Dicha transferencia se llevará a cabo previo consentimiento de su titular.
Usted puede manifestar la negativa a dicha transferencia, en cuyo caso no se generará el usuario y contraseña en la Plataforma, consecuentemente no se podrá acceder a la misma.

	</p>
	<p>
	<a id='read-more' class="more" href="#">Leer más</a>
	</p>
	<p>
	<input style="width: 15px;" type="checkbox" id="terms" name="terms" value="Bike">He leído y acepto los términos y condiciones<br>
	</p>
	
	<p>
	<input class="form-submit" type="submit" name="op" value="Enviar">
	</p>
	
</form>

<script type="text/javascript">
$(document).ready(function(){
	console.log("hello");
	$(':input[type="submit"]').prop('disabled', true);
	$('#terms').on('change',function(){
		var value = $('terms');
		if(document.getElementById('terms').checked){
			console.log(1);
			$(':input[type="submit"]').prop('disabled', false);
		}else{
			console.log(0);
			$(':input[type="submit"]').prop('disabled', true);
		}
	});
	$('#read-more').click(function(event){
		event.preventDefault();
		var linkClass = $("#read-more").attr("class");
		var shortText = "La Secretaría de Administración del Gobierno del Estado de Nuevo León, a través de la Dirección de Recursos Humanos, transferirá los datos personales que se detallan en el aviso de privacidad integral, los cuales se encuentran en los archivos físicos y bases de datos de esta Dirección, con la finalidad de acceder a la Plataforma ADN-GOB y los servicios que prestan en ella mediante la creación de un usuario y contraseña. Dicha transferencia se llevará a cabo previo consentimiento de su titular. Usted puede manifestar la negativa a dicha transferencia, en cuyo caso no se generará el usuario y contraseña en la Plataforma, consecuentemente no se podrá acceder a la misma.";
		var longText = "<p>AVISO DE PRIVACIDAD INTEGRAL</p> \n La Secretaría de Administración del Gobierno del Estado de Nuevo León, a través de la Dirección de Recursos Humanos de la Subsecretaría de Administración, con domicilio en el Edificio Biblioteca Central ubicado en calle Zuazua número 655 sur piso 8, Colonia Centro, municipio de Monterrey, Nuevo León, México, código postal 64000, es la autoridad responsable del tratamiento de sus datos personales. <br>La Secretaría de Administración, a través de la Dirección de Recursos Humanos, transferirá los datos personales que se detallan en éste aviso de privacidad integral, los cuales se encuentran en los archivos físicos y bases de datos de esta Dirección, con la finalidad de acceder a la Plataforma ADN-GOB y los servicios que prestan en ella mediante la creación de un usuario y contraseña. Dicha transferencia se llevará a cabo previo consentimiento de su titular. <br>Los datos personales a tratarse, son los siguientes: <br>1.- Número de empleado <br>2.- Nombre completo <br>3.- Dependencia <br>4.-  Percepción bruta y neta <br>La Secretaría de Administración tratará los datos personales antes señalados con fundamento en lo dispuesto en el artículo 19 fracciones  VII, X, XII, XXIII y XXV del Reglamento Interior de la Secretaría de Administración, así como lo dispuesto en los artículos 1, 3, 4, 6, 7, 16 al 30, 50 al 71 y demás relativos de la Ley General de Protección de Datos Personales en Posesión de Sujetos Obligados, y por los artículos 3 fracción XVI, XXXII, 23, 91, 145 y demás relativos de la Ley de Transparencia y Acceso a la Información Pública del Estado de Nuevo León.  <br>Usted podrá ejercer sus derechos de acceso, rectificación, cancelación u oposición de sus datos personales (derechos ARCO) directamente ante la Unidad de Transparencia de la Secretaría de Administración, en el edificio Biblioteca Central ubicado en calle Zuazua número 655 sur piso 9, Colonia Centro, municipio de Monterrey, Nuevo León, México, código postal 64000, o; a través de la Plataforma Nacional de Transparencia en la siguiente liga electrónica: www.plataformadetransparencia.org.mx, en el apartado de Solicitudes de Acceso a la Información; o bien, mediante el correo electrónico transparencia.admin@nuevoleon.gob.mx Los procedimientos para ejercer los derechos ARCO se encuentran previstos en los Capítulos I y II del Título Tercero de la Ley General de Protección de Datos Personales en Posesión de Sujetos Obligados. <br>Si tiene alguna duda sobre el ejercicio de sus derechos ARCO puede acudir a la Unidad de Transparencia de esta Secretaria de Administración, enviar un correo electrónico a la dirección antes señalada o comunicarse al teléfono +52 (81) 2020 1699. En caso de que exista un cambio de este aviso de privacidad, lo haremos de su conocimiento a través de nuestro sitio electrónico, en la liga electrónica: ________________________";
		// console.log($("#terms-body").text());
		if(linkClass == "more"){
			console.log(linkClass);
			$("#read-more").toggleClass("more");
			$("#read-more").toggleClass("less");
			$("#read-more").text("Leer menos");
			$("#terms-body").html(longText);
		}else{
			console.log(linkClass);
			$("#read-more").toggleClass("more");
			$("#read-more").toggleClass("less");
			$("#read-more").text("Leer más");
			$("#terms-body").text(shortText);
		}
	})
});

</script>

</div>
<div class="footer">
  <h2 class="url">www.adngob.mx</h2>
	<p>ADN Gob es una plataforma propiedad de Bison Tecnologies S.A.P.I. de C.V.</p>
	<p>Todos los derechos reservados &copy; 2018 Bisontec</p>
</div>

</body>
</html>
