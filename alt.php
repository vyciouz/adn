<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">

    <title>ADN</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">

    <meta name="MobileOptimized" content="width">
    <meta name="HandheldFriendly" content="true">
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="cleartype" content="on">
    
    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link href="style2.css" rel="stylesheet">
    <style>
        .smallTitle{
            font-weight: bold;
        }
        .desc{
            font-size: 12px;
        }

        .section{
            margin-top: 60px;
            margin-bottom: 60px;
        }
        .left{
            padding-left: 0;
        }
        .smallBox{
            text-align: center; 
        }

        .header{
            z-index: 9999;
            box-shadow: 0px 0px 3px 1px #000;
        }
    </style>
</head>
<body>
<div class = "container-fluid">
    <div class = "row">
        <div class = "col-md-12 header">
            HOLA
        </div>
    </div>
    <div class = "row">
        <div class = "col-md-2 side">
        </div>
        <div class = "col-md-8">
            <!-- Inicio del mugrero -->
            <div class = "row section" style = "text-align: center;">
                <div class = "col-md-12">
                    <img src="./img/escudo.svg" width="54" alt="">
                </div>
                <div class = "col-md-12">
                    Tenemos buenas noticias para ti
                </div>
            </div>
            <div class = "row" style = "text-align: center;">
                <div class = "col-md-12">
                    <img src="./img/adncover.png" style = "width: 70%" alt="">
                </div>
                <div class = "col-md-12">
                    <p>Muy pronto podrás gozar de los beneficios de ADN Gob. </p>
	                <p>A través de esta plataforma podrás acceder a créditos, solicitar efectivo, contratar seguros y muchos servicios más.</p>
                </div>
            </div>
            <div class = "row section">
                <?php
                $smallBoxes = [
                    "b1" => [
                        "img"=>"img/gastos.png",
                        "title"=>"Controla tus gastos",
                        "text"=>"Evita comprometer ingresos futuros, mejora tu estabilidad económica y planeación financiera."
                    ],
                    "b2" => [
                        "img"=>"img/tasas.png",
                        "title"=>"Obtén las mejores tasas",
                        "text"=>"Compara entre diferentes opciones para encontrar las mejores condiciones."
                    ],
                    "b3" => [
                        "img"=>"img/intermediarios.png",
                        "title"=>"Evita intermediarios",
                        "text"=>"Acceso a servicios desde cualquier lugar, muy práctico y conveniente."
                    ],

                ];?>
                <?php
                foreach($smallBoxes as $key => $value): ?>
                <div class = "col-md-4 smallBox section">
                    <div class = "col-md-12">
                        <img class="left" src="<?php echo $value["img"]?>" alt=""/>
                    </div>
                    <div class = "col-md-12 smallTitle">
                        <?php echo $value["title"]?>
                    </div>
                    <div class = "col-md-12 desc">
                        <?php echo $value["text"]?>
                    </div>
                </div>
                <?php endforeach ?>
            </div>
            
            <div class = "row section">
                <?php
                $mediumBoxes = [
                    "b1" => [
                        "img"=>"./img/img1.png",
                        "title"=>"Paynom",
                        "text"=>"Para cubrir cualquier imprevisto, podrás disponer de efectivo de forma inmediata, realizar pagos de diversos servicios y hasta recargar tiempo aire."
                    ],
                    "b2" => [
                        "img"=>"./img/img2.png",
                        "title"=>"Créditos",
                        "text"=>"Podrás comparar entre diferentes opciones para encontrar la mejor tasa y el plazo que te convenga. Hecho a tu medida."
                    ],
                    "b3" => [
                        "img"=>"./img/img3.png",
                        "title"=>"Seguros",
                        "text"=>"Excelentes tarifas y planes en una variedad de pólizas, desde auto, vida o funerarios. Todo con tu nómina."
                    ],

                ];
                $cont = 1;
                ?>
                <div class = "col-md-2">
                </div>
                <div class = "col-md-8">
                <?php foreach($mediumBoxes as $key => $value):?>
                    <div class = "row section">
                        <div class = "col-md-6" style = "flex: 2">
                            <div class = "col-md-12 smallTitle">
                                <?php echo $value["title"] ?>
                            </div>
                            <div class = "col-md-12 desc">
                                <?php echo $value["text"] ?>
                            </div>
                        </div>
                        <div class = "col-md-6 left" style = "flex: 2">
                            <img src="<?php echo $value["img"] ?>" style="width: 100%; max-width: 400px;" alt="">
                        </div>
                        
                    </div>
                <?php endforeach?>
                </div>
                <div class = "col-md-2">
                </div>
                
            </div>
            
            <!-- Fin del mugrero -->
        </div>
        <div class = "col-md-2 side">
        </div>
    </div>
    <div class = "row footer">
        <div class="col-md-12">
            <h2 class="url">www.adngob.mx</h2>
            <p>ADN Gob es una plataforma propiedad de Bison Tecnologies S.A.P.I. de C.V.</p>
            <p>Todos los derechos reservados &copy; 2018 Bisontec</p>
        </div>
    </div>
</div>

</body>
</html>