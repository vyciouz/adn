<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>ADN</title>
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<link href="style.css" rel="stylesheet">
</head>

<body>
	<img class="logo" src="img/logo.png" width="54" height="67" alt=""/>
	<h1 class="titulo">¡Tenemos buenas noticias para ti!</h1>
	
	<img src="img/adncover.png" class="cover" />
	<div class="txtcover">
	
		<p>Muy pronto podrás gozar de los beneficios de ADN Gob. </p>
	  <p>A través de esta plataforma podrás acceder a créditos, solicitar efectivo, contratar seguros y muchos servicios más.</p>
	</div>
	
<div class="iconos">
	
	
		<div class="col-3 gasto"><img src="img/gastos.png" width="68" height="71" alt=""/><h3 class="icontit">Controla tus gastos</h3><p>Evita comprometer ingresos futuros, mejora tu estabilidad económica y planeación financiera.</p></div>
		<div class="col-3 tasas"><img src="img/tasas.png" width="68" height="69" alt=""/><h3 class="icontit">Obtén las mejores tasas</h3><p>Compara entre diferentes opciones para encontrar las mejores condiciones.</p></div>
		<div class="col-3 intermediarios"><img src="img/intermediarios.png" width="59" height="64" alt=""/><h3 class="icontit">Evita intermediarios</h3><p>Acceso a servicios desde cualquier lugar, muy práctico y conveniente.</p></div>
	
	
</div>

	
<div class="iconos abajo">
	<div class="clearfix imagenes">
	<img class="left" src="img/img1.png" alt=""/>
	<div class="envolvente"><h3 class="icontit">Paynom</h3>
	<p>Para cubrir cualquier imprevisto, podrás disponer de efectivo de forma inmediata, realizar pagos de diversos servicios y hasta recargar tiempo aire.</p></div>
  </div>
	
	<div class="clearfix imagenes">
	<img class="right" src="img/img2.png" alt=""/>
	<div class="envolvent"><h3 class="icontit">Créditos</h3>
	<p>Podrás comparar entre diferentes opciones para encontrar la mejor tasa y el plazo que te convenga. Hecho a tu medida.</p></div>
  </div>
	
	<div class="clearfix imagenes">
	<img class="left" src="img/img3.png" alt=""/>
	<div class="envolvente"><h3 class="icontit">Seguros</h3>
	<p>Excelentes tarifas y planes en una variedad de pólizas, desde auto, vida o funerarios. Todo con tu nómina.</p></div>
	</div>
</div>
	
	
	
<a href="form.php" class="register">Registrate</a>	
	
<div class="footer">
	
	
  <h2 class="url">www.adngob.mx</h2>
	<p>ADN Gob es una plataforma propiedad de Bison Tecnologies S.A.P.I. de C.V.</p>
	<p>Todos los derechos reservados &copy; 2018 Bisontec</p>
</div>
	
</body>
</html>
