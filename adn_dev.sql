/*
Navicat MySQL Data Transfer

Source Server         : localDBs
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : adn_dev

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2018-08-06 12:52:07
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `cat_bancos`
-- ----------------------------
DROP TABLE IF EXISTS `cat_bancos`;
CREATE TABLE `cat_bancos` (
  `ID` int(11) NOT NULL,
  `NOMBRE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of cat_bancos
-- ----------------------------

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `ID_USER` int(11) NOT NULL AUTO_INCREMENT,
  `NOMBRE` varchar(255) DEFAULT NULL,
  `APELLIDO_P` varchar(255) DEFAULT NULL,
  `APELLIDO_M` varchar(255) DEFAULT NULL,
  `EMAIL` varchar(255) DEFAULT NULL,
  `EMAIL_ALT` varchar(255) DEFAULT NULL,
  `BANCO` int(11) DEFAULT NULL,
  `CUENTA` varchar(255) DEFAULT NULL,
  `CLABE` varchar(255) DEFAULT NULL,
  `RFC` varchar(255) DEFAULT NULL,
  `FECHA_CREADO` date DEFAULT NULL,
  `PASSWORD` varchar(255) NOT NULL,
  PRIMARY KEY (`ID_USER`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'VICTOR', 'MENDOZA', 'ROCHA', 'EMAIL@COM', 'EMAIL@COM', '0', '1', '2', 'MERV', '2018-08-06', '');
INSERT INTO `user` VALUES ('2', 'VICTOR', 'MENDOZA', 'ROCHA', 'EMAIL@COM', 'EMAIL@COM', '0', '1', '2', 'MERV', '2018-08-06', '');
INSERT INTO `user` VALUES ('3', 'VICTOR', 'MENDOZA', 'ROCHA', 'EMAIL@COM', 'EMAIL@COM', '0', '1', '2', 'MERV', '2018-08-06', '');
